#!/usr/bin/env python3

import json


def add_ligand_record(ligands, lig_name, counter, x, y, z, cc, res, chain):
    if lig_name != "HOH":
        # HOH is ommited 
        #12345678901234567890123456789012345678901234567890123456789012345678901234567890
        #HETATM 3835 FE   HEM A   1      17.140   3.115  15.066  1.00 14.14          FE
        #HETATM 8238  S   SO4 A2001      10.885 -15.746 -14.404  1.00 47.84           S  
        d = {"chain": chain,
             "auth_chain": chain,
             "n_atoms": counter,
             "center": [x/counter,y/counter, z/counter],
             "entity": cc,
             "entity_type":"?",
             "entity_comp": lig_name,
             "entity_description":"",
             "res": res
             }
        ligands[cc]=(d)


def filter_empty(res):
    r = []
    for i in res:
        if i != "":
            r.append(i)
    return r


def generate_ligand_json(cif_filename, ligand_filename):
    ligands = {}

    pos = {}

    # at first read the record positions:
    with open(cif_filename,"r") as fcif:
        counter = 0
        start = 0
        line = fcif.readline()
        while line:
            if line.startswith("loop_"):
                start = counter+1
            if (line.startswith("_atom_site.Cartn_x")):
                pos["x"] = counter - start
            if (line.startswith("_atom_site.Cartn_y")):
                pos["y"] = counter - start
            if (line.startswith("_atom_site.Cartn_z")):
                pos["z"] = counter - start
            if (line.startswith("_atom_site.label_entity_id")):
                pos["res"] = counter - start
            if (line.startswith("_atom_site.auth_asym_id")):
                pos["chain"] = counter - start
            if (line.startswith("_atom_site.label_comp_id")):
                pos["name"] = counter - start

            counter += 1
            line = fcif.readline()
            if len(pos) == 6:
                break

    with open(cif_filename,"r") as fcif:
        lig_name = ""
        lig_number = -1
        x, y, z, counter = 0, 0, 0, 0
        cc = 1

        line = fcif.readline()
        lig_chain = ""
        lig_number = 0

        while line:
            if line.startswith("HETATM"):
                s = line.split(" ")
                ss = filter_empty(s)
                # split input HETATMrecord to parts divided by spaces,
                # there can be more spaces between each record
                # line HETATM 3767 C CHA HEM . . . B 2 -15.56 -19.721 -10.646 1 33.1 ? CHA HEM 508 A 1 508 ? ? ? ? 
                # line HETATM 3768 C CHB HEM . . . B 2 -16.452 -22.296 -14.515 1 29.45 ? CHB HEM 508 A 1 508 ? ? ? ? 

                if lig_number != ss[pos["res"]] or lig_chain != ss[pos["chain"]]:
                    if lig_name != "":
                        # one ligand data is already red, add relevant record
                        add_ligand_record(ligands, lig_name, counter, x, y, z, cc, int(lig_number), lig_chain)

                        x, y, z, counter = 0, 0, 0, 0
                        cc += 1
                    lig_number = ss[pos["res"]]
                    lig_name = ss[pos["name"]]
                    lig_chain = ss[pos["chain"]]

                x += float(ss[pos["x"]])
                y += float(ss[pos["y"]])
                z += float(ss[pos["z"]])

                counter += 1
            line = fcif.readline()

        if lig_name != "":
            # last ligand data are already red, add relevant record
            add_ligand_record(ligands, lig_name, counter, x, y, z, cc, int(lig_number), lig_chain)

    fd = open(ligand_filename, "w")
    json.dump(ligands, fd, separators=(',', ':'))

    return
