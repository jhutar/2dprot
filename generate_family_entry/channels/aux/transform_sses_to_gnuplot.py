#!/usr/bin/env python3
# go through unannotated channels and write which channel is the nearest one

import argparse
import json


def generate_gnuplot_splot_input(input_filename):
    with open(input_filename, "r") as fr:
        json_data = json.load(fr)
        chain_name = list(json_data.keys())[0]
        for rec in json_data[chain_name]["secondary_structure_elements"]:
            input_file_name = f"gnuplot-sses-{rec['label']}.txt"
            with open(input_file_name, "w") as fw:
                start = rec["start_vector"]
                end = rec["end_vector"]
                fw.write(f"{start[0]} {start[1]} {start[2]} \n")
                fw.write(f"{end[0]} {end[1]} {end[2]} \n")
            print(f"'{input_file_name}' lt rgb 'red'", end=", ")


# read arguments
parser = argparse.ArgumentParser(
    description='generate statistic regarding sses for domain from the input directory')
parser.add_argument('--directory', required=True, action="store",
                    help="domain annotation files directory")
parser.add_argument('--protein', required=True, action="store",
                    help="domain name")

args = parser.parse_args()


sses_layout_filename = f"{args.directory}/{args.protein}-annotated.sses.json"
generate_gnuplot_splot_input(sses_layout_filename)
print()
