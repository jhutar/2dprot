#!/usr/bin/env python3
# go through unannotated channels and write which channel is the nearest one

import argparse
import json
import sys
import numpy
sys.path.append("../..")
import utils_vector


def pnt2line(pnt, start, end):
    line_vec = utils_vector.rm(end, start)
    line_len = numpy.linalg.norm(line_vec)
    line_unitvec = numpy.linalg.norm(line_vec)
    pnt_vec = utils_vector.rm(pnt, start)
    pnt_vec_scaled = utils_vector.frac(pnt_vec, line_len)
    t = numpy.dot(line_unitvec, pnt_vec_scaled)
    if t < 0.0:
        t = 0.0
    elif t > 1.0:
        t = 1.0
    nearest = utils_vector.mult(line_vec, t)
    dist = numpy.linalg.norm(utils_vector.rm(pnt_vec, nearest))
    nearest = utils_vector.add(nearest, start)
    return (dist, nearest)


def read_file_list(filename):
    fl = []
    with open(filename, "r") as fd:
        line = fd.readline()
        while line:
            sline = line.split(" ")
            if len(sline) == 2:
                fl.append([sline[0], sline[1][:-1]])
            else:
                fl.append([sline[0], sline[1], sline[2][:-1]])
            line = fd.readline()
    return fl


def generate_gnuplot_splot_input(input_filename, channel_name, new_file_name):
    with open(new_file_name, "w") as fw:
        with open(input_filename, "r") as fr:
            json_data = json.load(fr)
            for ch in json_data["Annotations"]:
                if ch['Name'] == channel_name.replace("+", " "):
                    ch_id = ch['Id']
            for ch_type in json_data['Channels']:
                for ch in json_data['Channels'][ch_type]:
                    if ch['Id'] == ch_id:
                        for j in ch['Profile']:
                            fw.write(f"{j['X']} {j['Y']} {j['Z']} \n")
    print(f"'{new_file_name}' lt rgb 'green'", end=", ")


def generate_gnuplot_splot_input2(input_filename, ch_id, new_file_name, near, domain):
    is_near = True
    if near:
        # we have to test whether the channel is near enough <10A to
        # be shown
        is_near = False
        with open(input_filename, "r") as fr, open(near, "r") as fa:
            json_data_chan = json.load(fr)
            json_data_sses = json.load(fa)
            for ch_type in json_data_chan['Channels']:
                for ch in json_data_chan['Channels'][ch_type]:
                    if ch['Id'] == ch_id:
                        for j in ch['Profile']:
                            for sses in json_data_sses[domain]["secondary_structure_elements"]:
                                dist = pnt2line([j['X'], j['Y'], j['Z']], sses["start_vector"], sses["end_vector"])
                                if dist[0] < 15:
                                    is_near = True
    if not is_near:
        return
    with open(new_file_name, "w") as fw:
        with open(input_filename, "r") as fr:
            json_data = json.load(fr)
            for ch_type in json_data['Channels']:
                for ch in json_data['Channels'][ch_type]:
                    if ch['Id'] == ch_id:
                        for j in ch['Profile']:
                            fw.write(f"{j['X']} {j['Y']} {j['Z']} \n")
    print(f"'{new_file_name}' lt rgb 'orange'", end=", ")


# read arguments
parser = argparse.ArgumentParser(
    description='generate gnuplot input data for input channel annotation files from input directory')
parser.add_argument('--directory', required=True, action="store",
                    help="channel files directory")
parser.add_argument('--unknown_file_list', action="store",
                    help="file containing protein names and chain numbers of hanis which would be shown")
parser.add_argument('--named_file_list', action="store",
                    help="file containing protein names and chain numbers of hanis which would be shown")
parser.add_argument('--near', action="store_true",
                    help="we want to have only channels which are near to sses of the given domain <10A")

args = parser.parse_args()

if args.named_file_list:
    file_list = read_file_list(args.named_file_list)
    for i in file_list:
        threed_layout_filename = f"{args.directory}/{i[0]}-channel.json"
        generate_gnuplot_splot_input(threed_layout_filename, i[2], f"gnuplot-{i[0][:4]}{i[0][6:]}{i[0][4:6]}-{i[1]}.txt")
    print()

if args.unknown_file_list:
    file_list = read_file_list(args.unknown_file_list)
    for i in file_list:
        if len(i) > 2:
            output_filename = f"gnuplot-{i[0][:4]}{i[0][6:]}{i[0][4:6]}-{i[1]}-{i[2]}.txt"
        else:
            output_filename = f"gnuplot-{i[0][:4]}{i[0][6:]}{i[0][4:6]}-{i[1]}.txt"
        if args.near:
            near = f"{args.directory}/{i[0][:4]}{i[0][6:]}{i[0][4:6]}-annotated.sses.json"
        else:
            near = False
        threed_layout_filename = f"{args.directory}/{i[0]}-channel.json"
        generate_gnuplot_splot_input2(threed_layout_filename, i[1], output_filename, near, f"{i[0][:4]}{i[0][6:]}{i[0][4:6]}")
    print()
