# grep all channels with the name .. to one file
for i in `ls generated-1.10.630.10-2024-04-08T08_15_20/annotation/chan*`; do 
    for j in `grep -n "Channel 5" $i | sed "s|{|\n{|g" | grep "Channel 5" | sed 's|.*Id": "||' | sed 's|".*||'`; do 
        echo $i $j "' lt rgb 'Tunnel'" | sed "s|generated-1.10.630.10-2024-04-08T08_15_20/annotation/channel-|'gnuplot-1.10.630.10-2024-04-17T10_24_52/gnuplot-|" | sed "s|.json |00A.txt_|";
    done
done | sort | uniq 
