#!/usr/bin/bash

j="Channel 3"
flist=$1 #"lists/list_chan301_40_s_cl_1"

for i in `grep "$j" generated-1.10.630.10-2024-04-08T08_15_20/annotation/*-channel* | sed "s|.*/annotation/||" | sed "s|-channel.json.*||" | sort | uniq`; do 
    grep $i $flist >/dev/null;
    if [ $? -ne 0 ]; then 
       j=`grep -n1n "$j"  generated-1.10.630.10-2024-04-08T08_15_20/annotation/$i-channel.json | grep Id `
       echo "possible channel:" $i $j

    fi
done
echo $flist