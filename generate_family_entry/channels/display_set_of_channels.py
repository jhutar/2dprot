#!/usr/bin/env python3
import mayavi.mlab
import argparse


def read_file_list(filename):
    fl = []
    with open(filename, "r") as fd:
        line = fd.readline()
        while line:
            sline = line.split(" ")
            fl.append([sline[0], sline[1], sline[2][:-1]])
            line = fd.readline()
    return fl


def generate_axis_set(ch_filename):
    axis = {'x': [],
            'y': [],
            'z': [],
            }
    with open(ch_filename, "r") as fd:
        line = fd.readline()
        while line:
            sline = line.split(" ")
            axis['x'].append(float(sline[0]))
            axis['y'].append(float(sline[1]))
            axis['z'].append(float(sline[2]))
            line = fd.readline()
    return axis


def generate_mayavi_input(filename, radius, g_color, grafik, directory):

    with open(filename, "r") as fd:
        line = fd.readline()
        while line:
            sline = line.strip().split(",")
            for rec in sline:
                srec = rec.split("'")
                if len(srec) < 2:
                    continue
                fn = srec[1]
                ch_filename = f"{directory}/{fn}"

                axis = generate_axis_set(ch_filename)
                s_color = g_color.split(",")
                c = (float(s_color[0]), float(s_color[1]), float(s_color[2]))
                if radius == "None":
                    r = None
                else:
                    r = float(radius)

                if color == "1,0,0":
##                    mayavi.mlab.plot3d(axis['x'], axis['y'], axis['z'], figure=grafik, tube_radius=r, color=c, name=fn)
                      mayavi.mlab.text3d(axis['x'][-1], axis['y'][-1], axis['z'][-1], fn[-17:], scale=0.1)
#                else:
#                mayavi.mlab.plot3d(axis['x'][-3:], axis['y'][-3:], axis['z'][-3:], figure=grafik, tube_radius=r, color=c, name=fn)
                mayavi.mlab.plot3d(axis['x'], axis['y'], axis['z'], figure=grafik, tube_radius=r, color=c, name=fn)
#                mayavi.mlab.text3d(axis['x'][-1], axis['y'][-1], axis['z'][-1], fn[-17:], scale=0.2)
#                if fn[-17:].startswith("3r1b"):
#                  mayavi.mlab.plot3d(axis['x'], axis['y'], axis['z'], figure=grafik, tube_radius=0.5, color=(0,1,0), name=fn)
#                  mayavi.mlab.text3d(axis['x'][-1], axis['y'][-1], axis['z'][-1], fn[-17:], scale=0.4)

#                    mayavi.mlab.text3d(axis['x'][1], axis['y'][1], axis['z'][1], fn[8:-4], scale=0.2)
            line = fd.readline()


# read arguments
parser = argparse.ArgumentParser(
    description='show input channels and sses files using mayavi tool')
parser.add_argument('--directory', required=True, action="store",
                    help="channel/sses gnuplot files directory")
parser.add_argument('--file_list', required=True, action="store",
                    help="file containing protein names and chain numbers of chanis which would be shown")
args = parser.parse_args()


file_list = read_file_list(args.file_list)
grafik = mayavi.mlab.figure()
for i in file_list:
    file_name = i[0]
    radius = i[1]
    color = i[2]
    generate_mayavi_input(file_name, radius, color, grafik, args.directory)

setup = "all"
# protein 4h6o - H360, H361, E78,1 H22, 1  [-13.802, 18.641, -4.972]
if setup == "all":
    #3g5n - H36
    k=[[-20.909, 25.683, 0.199], [0.816, 9.588, -13.227]]
    mayavi.mlab.text3d((k[0][0]+k[1][0])/2, (k[0][1]+k[1][1])/2, (k[0][2]+k[1][2])/2, "H36 - C", scale=1)
    mayavi.mlab.plot3d([k[0][0], k[1][0]], [k[0][1], k[1][1]], [k[0][2], k[1][2]], figure=grafik, tube_radius=0.5, color=(0.3, 0.3, 0.3), name="49")

    #3g5n - H22
    k=[[-16.286, -13.31, -7.093],[-19.496, 0.521, -12.31]]
    mayavi.mlab.text3d((k[0][0]+k[1][0])/2, (k[0][1]+k[1][1])/2, (k[0][2]+k[1][2])/2, "H22 - B", scale=1)
    mayavi.mlab.plot3d([k[0][0], k[1][0]], [k[0][1], k[1][1]], [k[0][2], k[1][2]], figure=grafik, tube_radius=0.5, color=(0.3, 0.3, 0.3), name="49")

    #3g5n - H78
    k=[[-17.162, 1.267, 1.133], [-22.073, 3.272, -1.162]]
    mayavi.mlab.text3d((k[0][0]+k[1][0])/2, (k[0][1]+k[1][1])/2, (k[0][2]+k[1][2])/2, "E78 ", scale=1)
    mayavi.mlab.plot3d([k[0][0], k[1][0]], [k[0][1], k[1][1]], [k[0][2], k[1][2]], figure=grafik, tube_radius=0.5, color=(0.3, 0.3, 0.3), name="49")

    # 1tqn - protein
    # H36
    k = [[-15.234, 17.586, -4.858], [3.405, 12.619, -13.714]]
    mayavi.mlab.text3d((k[0][0]+k[1][0])/2, (k[0][1]+k[1][1])/2, (k[0][2]+k[1][2])/2, "H36 - C", scale=1)
    mayavi.mlab.plot3d([k[0][0], k[1][0]], [k[0][1], k[1][1]], [k[0][2], k[1][2]], figure=grafik, tube_radius=0.4, color=(0.0, 0.0, 0.0), name="49")

    # E78
    k = [[-11.187, -2.062, 0.463], [-20.463, 5.905, -0.583]]
    mayavi.mlab.text3d((k[0][0]+k[1][0])/2, (k[0][1]+k[1][1])/2, (k[0][2]+k[1][2])/2, "E78", scale=1)
    mayavi.mlab.plot3d([k[0][0], k[1][0]], [k[0][1], k[1][1]], [k[0][2], k[1][2]], figure=grafik, tube_radius=0.4, color=(0.0, 0.0, 0.0), name="49")

    # H22
    k = [[-19.28, -9.771, -9.262], [-17.46, 4.832, -10.757]]
    mayavi.mlab.text3d((k[0][0]+k[1][0])/2, (k[0][1]+k[1][1])/2, (k[0][2]+k[1][2])/2, "H22 - B", scale=1)
    mayavi.mlab.plot3d([k[0][0], k[1][0]], [k[0][1], k[1][1]], [k[0][2], k[1][2]], figure=grafik, tube_radius=0.4, color=(0.0, 0.0, 0.0), name="49")

    # H13
    k = [[-8.643, -9.326, 10.666], [-22.507, -17.796, 3.49]]
    mayavi.mlab.text3d((k[0][0]+k[1][0])/2, (k[0][1]+k[1][1])/2, (k[0][2]+k[1][2])/2, "H13 - A", scale=1)
    mayavi.mlab.plot3d([k[0][0], k[1][0]], [k[0][1], k[1][1]], [k[0][2], k[1][2]], figure=grafik, tube_radius=0.4, color=(0.0, 0.0, 0.0), name="49")

    # H28 - from 1akd (1tqn do not have)
    k = [ [-15.301, 6.28, 12.978], [-12.006, 13.195, 7.202]]
    mayavi.mlab.text3d((k[0][0]+k[1][0])/2, (k[0][1]+k[1][1])/2, (k[0][2]+k[1][2])/2, "H28 - B'", scale=1)
    mayavi.mlab.plot3d([k[0][0], k[1][0]], [k[0][1], k[1][1]], [k[0][2], k[1][2]], figure=grafik, tube_radius=0.4, color=(0.0, 0.0, 0.0), name="49")

    # H 41 - C'
    k = [[8.252, 10.671, -16.343], [30.725, -9.707, 10.426]]
    mayavi.mlab.text3d((k[0][0]+k[1][0])/2, (k[0][1]+k[1][1])/2, (k[0][2]+k[1][2])/2, "H41 - D", scale=1)
    mayavi.mlab.plot3d([k[0][0], k[1][0]], [k[0][1], k[1][1]], [k[0][2], k[1][2]], figure=grafik, tube_radius=0.4, color=(0.0, 0.0, 0.0), name="49")

    # H 46 - E
    k = [[13.383, -7.271, 10.249], [12.997, 14.235, -3.901]]
    mayavi.mlab.text3d((k[0][0]+k[1][0])/2, (k[0][1]+k[1][1])/2, (k[0][2]+k[1][2])/2, "H46 - E", scale=1)
    mayavi.mlab.plot3d([k[0][0], k[1][0]], [k[0][1], k[1][1]], [k[0][2], k[1][2]], figure=grafik, tube_radius=0.4, color=(0.0, 0.0, 0.0), name="49")

    # H 49 - F
    k = [[16.464, 14.458, 10.76], [3.736, 6.82, 10.995]]
    mayavi.mlab.text3d((k[0][0]+k[1][0])/2, (k[0][1]+k[1][1])/2, (k[0][2]+k[1][2])/2, "H49 - F", scale=1)
    mayavi.mlab.plot3d([k[0][0], k[1][0]], [k[0][1], k[1][1]], [k[0][2], k[1][2]], figure=grafik, tube_radius=0.4, color=(0.0, 0.0, 0.0), name="49")

    # H 59 - G
    k = [[2.482, 16.054, 15.711], [17.205, 30.394, -8.035]]
    mayavi.mlab.text3d((k[0][0]+k[1][0])/2, (k[0][1]+k[1][1])/2, (k[0][2]+k[1][2])/2, "H59 - G", scale=1)
    mayavi.mlab.plot3d([k[0][0], k[1][0]], [k[0][1], k[1][1]], [k[0][2], k[1][2]], figure=grafik, tube_radius=0.4, color=(0.0, 0.0, 0.0), name="49")

    # H 63 - H
    k = [[9.925, 15.518, -5.467], [2.296, 26.203, -11.997]]
    mayavi.mlab.text3d((k[0][0]+k[1][0])/2, (k[0][1]+k[1][1])/2, (k[0][2]+k[1][2])/2, "H63 - H", scale=1)
    mayavi.mlab.plot3d([k[0][0], k[1][0]], [k[0][1], k[1][1]], [k[0][2], k[1][2]], figure=grafik, tube_radius=0.4, color=(0.0, 0.0, 0.0), name="49")

    # H 68 - I
    k = [[-1.709, 25.521, 1.307], [11.375, -21.297, 2.206]]
    mayavi.mlab.text3d((k[0][0]+k[1][0])/2, (k[0][1]+k[1][1])/2, (k[0][2]+k[1][2])/2, "H68 - I", scale=1)
    mayavi.mlab.plot3d([k[0][0], k[1][0]], [k[0][1], k[1][1]], [k[0][2], k[1][2]], figure=grafik, tube_radius=0.4, color=(0.0, 0.0, 0.0), name="49")

    # H70 - J
    k = [[11.116, -22.734, -2.722], [21.227, -10.549, -17.114]]
    mayavi.mlab.text3d((k[0][0]+k[1][0])/2, (k[0][1]+k[1][1])/2, (k[0][2]+k[1][2])/2, "H70 - J", scale=1)
    mayavi.mlab.plot3d([k[0][0], k[1][0]], [k[0][1], k[1][1]], [k[0][2], k[1][2]], figure=grafik, tube_radius=0.4, color=(0.0, 0.0, 0.0), name="49")

    # H73 - K
    k = [[10.587, -12.647, -14.704], [-2.818, -10.981, 0.219]]
    mayavi.mlab.text3d((k[0][0]+k[1][0])/2, (k[0][1]+k[1][1])/2, (k[0][2]+k[1][2])/2, "H73 - K", scale=1)
    mayavi.mlab.plot3d([k[0][0], k[1][0]], [k[0][1], k[1][1]], [k[0][2], k[1][2]], figure=grafik, tube_radius=0.4, color=(0.0, 0.0, 0.0), name="49")

    # H94 - L
    k = [[1.613, 3.944, -8.834], [23.564, -11.698, -3.958]]
    mayavi.mlab.text3d((k[0][0]+k[1][0])/2, (k[0][1]+k[1][1])/2, (k[0][2]+k[1][2])/2, "H94 - L", scale=1)
    mayavi.mlab.plot3d([k[0][0], k[1][0]], [k[0][1], k[1][1]], [k[0][2], k[1][2]], figure=grafik, tube_radius=0.4, color=(0.0, 0.0, 0.0), name="49")


pom = [[-16.87282779170141, 2.527273383466011, -10.567140340889953], [-21.075370613078462, 7.137765435295564, -9.103088194616227], [-21.48985783073343, 3.4906188521980113, -4.724650430739785], [-14.920763079780393, 8.0156786000684, 0.7491564049258256], [-19.79897726299646, 10.947550234721778, 3.1780916677049165], [-15.318300921191575, 13.979684195119003, 6.47822649928824], [-21.649994191969306, 8.800793532882448, 7.993654297686316], [-16.030273523094834, 5.437990328737956, 8.596212813673604], [-14.087767931567358, 2.4513494762546943, 12.979259778415944], [-7.375231248265336, 1.580185440640779, 15.382900106950153], [-8.559003513059558, 5.958206777148474, 16.000975275164933], [-7.903485292764683, 8.7622374057138, 9.821239774467369], [-9.118910264126368, 12.657203287194427, 4.541763612859076], [-7.156224832297244, 12.362048784186195, -2.5464085419555675], [-12.141751586718238, 12.17045800245346, 1.2441440771447887], [-16.08932798802225, 12.912322871218091, -4.223460466335678], [-16.016204125087096, 13.765848711515222, -8.187731504812659], [-15.008, 16.764, -7.043], [-17.397, 3.068, -8.739]]
i = 0
while i+1 < len(pom):
    mayavi.mlab.plot3d([pom[i][0], pom[i+1][0]], [pom[i][1], pom[i+1][1]], [pom[i][2], pom[i+1][2]], figure=grafik, tube_radius=0.2, color=(0.0, 1.0, 0.0), name=f"<{i}>")
    i += 1
#======

pom = [[0.16049382519669422, 11.222691990804162, 21.349395523469887], [4.326372712129789, 6.46004341299122, 20.11581105435684], [3.6318505604846836, 2.7105006467195523, 16.707444720406407], [-0.14839861301517843, 1.3193855917174846, 12.867327467422216], [3.537327995921659, 9.240504537485617, 10.991336662133902], [4.468, 5.721, 14.164], [-20.709, -4.859, 12.919], [-20.709, -4.859, 12.919], [-9.118910264126368, 12.657203287194427, 4.541763612859076], [-9.118910264126368, 12.657203287194427, 4.541763612859076], [1.981, 11.369, 23.501]]
i = 0
while i+1 < len(pom):
    mayavi.mlab.plot3d([pom[i][0], pom[i+1][0]], [pom[i][1], pom[i+1][1]], [pom[i][2], pom[i+1][2]], figure=grafik, tube_radius=0.2, color=(1.0, 0.0, 0.0), name=f"<{i}>")
    i += 1

pom = [[-2.044, 26.419, 0.892], [12.232, 27.828, -6.961], [12.232, 27.828, -6.961], [1.981, 11.369, 23.501], [1.981, 11.369, 23.501], [-9.118910264126368, 12.657203287194427, 4.541763612859076], [-9.118910264126368, 12.657203287194427, 4.541763612859076], [-2.044, 26.419, 0.892]]
i = 0
while i+1 < len(pom):
    mayavi.mlab.plot3d([pom[i][0], pom[i+1][0]], [pom[i][1], pom[i+1][1]], [pom[i][2], pom[i+1][2]], figure=grafik, tube_radius=0.2, color=(1.0, 0.0, 0.0), name=f"<{i}>")
    i += 1

# ======

pom = [[0.16049382519669422, 11.222691990804162, 21.349395523469887], [4.326372712129789, 6.46004341299122, 20.11581105435684], [3.6318505604846836, 2.7105006467195523, 16.707444720406407], [-0.14839861301517843, 1.3193855917174846, 12.867327467422216], [3.537327995921659, 9.240504537485617, 10.991336662133902], [4.468, 5.721, 14.164], [-20.709, -4.859, 12.919], [-20.709, -4.859, 12.919], [-7.375231248265336, 1.580185440640779, 15.382900106950153], [-7.375231248265336, 1.580185440640779, 15.382900106950153], [1.981, 11.369, 23.501]]
i = 0
while i+1 < len(pom):
    mayavi.mlab.plot3d([pom[i][0], pom[i+1][0]], [pom[i][1], pom[i+1][1]], [pom[i][2], pom[i+1][2]], figure=grafik, tube_radius=0.3, color=(0.6, 0.0, 0.3), name=f"<{i}>")
    i += 1

pom= [[0.16049382519669422, 11.222691990804162, 21.349395523469887], [4.326372712129789, 6.46004341299122, 20.11581105435684], [3.6318505604846836, 2.7105006467195523, 16.707444720406407], [-0.14839861301517843, 1.3193855917174846, 12.867327467422216], [3.537327995921659, 9.240504537485617, 10.991336662133902], [4.468, 5.721, 14.164], [-20.709, -4.859, 12.919], [-20.709, -4.859, 12.919], [-12.045960371692553, 8.258124182344774, 12.189539182645488], [-12.045960371692553, 8.258124182344774, 12.189539182645488], [1.981, 11.369, 23.501]]
i = 0
while i+1 < len(pom):
    mayavi.mlab.plot3d([pom[i][0], pom[i+1][0]], [pom[i][1], pom[i+1][1]], [pom[i][2], pom[i+1][2]], figure=grafik, tube_radius=0.4, color=(0.3, 0.0, 0.6), name=f"<{i}>")
    i += 1
pom = [[-2.044, 26.419, 0.892], [12.232, 27.828, -6.961], [12.232, 27.828, -6.961], [1.981, 11.369, 23.501], [1.981, 11.369, 23.501], [-12.045960371692553, 8.258124182344774, 12.189539182645488], [-12.045960371692553, 8.258124182344774, 12.189539182645488], [-2.044, 26.419, 0.892]]
i = 0
while i+1 < len(pom):
    mayavi.mlab.plot3d([pom[i][0], pom[i+1][0]], [pom[i][1], pom[i+1][1]], [pom[i][2], pom[i+1][2]], figure=grafik, tube_radius=0.4, color=(0.3, 0.0, 0.6), name=f"<{i}>")
    i += 1


pom = [[0.16049382519669422, 11.222691990804162, 21.349395523469887], [4.326372712129789, 6.46004341299122, 20.11581105435684], [3.6318505604846836, 2.7105006467195523, 16.707444720406407], [-0.14839861301517843, 1.3193855917174846, 12.867327467422216], [3.537327995921659, 9.240504537485617, 10.991336662133902], [4.468, 5.721, 14.164], [-20.709, -4.859, 12.919], [-20.709, -4.859, 12.919], [-7.375231248265336, 1.580185440640779, 15.382900106950153], [-7.375231248265336, 1.580185440640779, 15.382900106950153], [1.981, 11.369, 23.501]]
i = 0
while i+1 < len(pom):
    mayavi.mlab.plot3d([pom[i][0], pom[i+1][0]], [pom[i][1], pom[i+1][1]], [pom[i][2], pom[i+1][2]], figure=grafik, tube_radius=0.5, color=(0.0, 0.0, 1.0), name=f"<{i}>")
    i += 1

    # H49 / 4wmwB00
k = [[17.649, 10.221, 12.264], [-8.923, 0.317, 13.4]]
mayavi.mlab.text3d((k[0][0]+k[1][0])/2, (k[0][1]+k[1][1])/2, (k[0][2]+k[1][2])/2, "H49 - F", scale=1)
mayavi.mlab.plot3d([k[0][0], k[1][0]], [k[0][1], k[1][1]], [k[0][2], k[1][2]], figure=grafik, tube_radius=0.4, color=(0.2, 0.2, 0.2), name="49")

#pom = [[-13.858285514626372, 9.162990597786376, -1.0118686729588724],[-6.829593770718813, 10.428142442775567, 7.641972173491343],[-8.747003875524637, 15.231879149614505, 8.161579025549868], [-13.858285514626372, 9.162990597786376, -1.0118686729588724]]
#i = 0
#while i+1 < len(pom):
#    mayavi.mlab.plot3d([pom[i][0], pom[i+1][0]], [pom[i][1], pom[i+1][1]], [pom[i][2], pom[i+1][2]], figure=grafik, tube_radius=0.1, color=(1.0, 0.0, 1.0), name=f"<{i}>")
#    i += 1
#
#pom = [[-13.858285514626372, 9.162990597786376, -1.0118686729588724],[-2.9599442823225095, 13.238280754944823, 8.982107757146265],[-3.631923407643194, 13.406373592840923, 5.006954147261044], [-13.858285514626372, 9.162990597786376, -1.0118686729588724]]
#i = 0
#while i+1 < len(pom):
#    mayavi.mlab.plot3d([pom[i][0], pom[i+1][0]], [pom[i][1], pom[i+1][1]], [pom[i][2], pom[i+1][2]], figure=grafik, tube_radius=0.1, color=(1.0, 0.0, 1.0), name=f"<{i}>")
#    i += 1


if setup == "channel3":
    # sses 3l4dC00 H49, H59
    mayavi.mlab.plot3d([16.663, -3.714], [11.112, 8.074], [7.05, 15.659], figure=grafik, tube_radius=1, color=(0.0, 0.0, 0.0), name="49")
    mayavi.mlab.plot3d([-4.17, 19.632], [12.427, 33.384], [23.427, -11.151], figure=grafik, tube_radius=1, color=(0.0, 0.0, 0.0), name="49")
    #trisngle 1
    k=[[-1.7898, 14.205, 19.9692], [-4.17, 12.074, 23.427], [-1.6763, 8.377799999999999, 14.798100000000002], [-3.714, 8.074, 15.659]]
    mayavi.mlab.plot3d([k[0][0], k[1][0]], [k[0][1], k[1][1]], [k[0][2], k[1][2]], figure=grafik, tube_radius=0.2, color=(0.0, 0.0, 0.9), name="49")
    mayavi.mlab.plot3d([k[2][0], k[0][0]], [k[2][1], k[0][1]], [k[2][2], k[0][2]], figure=grafik, tube_radius=0.2, color=(0.0, 0.0, 0.9), name="49")
    mayavi.mlab.plot3d([k[1][0], k[2][0]], [k[1][1], k[2][1]], [k[1][2], k[2][2]], figure=grafik, tube_radius=0.2, color=(0.0, 0.0, 0.9), name="49")

    k = [[7.731000000000001, 22.729, 6.138], [5.3508000000000004, 20.598, 9.5958], [6.4745, 9.593, 11.3545], [4.436800000000001, 9.289200000000001, 12.2154]]
    mayavi.mlab.plot3d([k[0][0], k[1][0]], [k[0][1], k[1][1]], [k[0][2], k[1][2]], figure=grafik, tube_radius=0.2, color=(0.0, 0.0, 0.9), name="49")
    mayavi.mlab.plot3d([k[2][0], k[0][0]], [k[2][1], k[0][1]], [k[2][2], k[0][2]], figure=grafik, tube_radius=0.2, color=(0.0, 0.0, 0.9), name="49")
    mayavi.mlab.plot3d([k[1][0], k[2][0]], [k[1][1], k[2][1]], [k[1][2], k[2][2]], figure=grafik, tube_radius=0.2, color=(0.0, 0.0, 0.9), name="49")

    k = [[19.632, 33.384, -11.151], [17.2518, 31.253, -7.6932], [16.663, 11.112, 7.05], [14.625300000000001, 10.8082, 7.9109]]
    mayavi.mlab.plot3d([k[0][0], k[1][0]], [k[0][1], k[1][1]], [k[0][2], k[1][2]], figure=grafik, tube_radius=0.2, color=(0.0, 0.0, 0.9), name="49")
    mayavi.mlab.plot3d([k[2][0], k[0][0]], [k[2][1], k[0][1]], [k[2][2], k[0][2]], figure=grafik, tube_radius=0.2, color=(0.0, 0.0, 0.9), name="49")
    mayavi.mlab.plot3d([k[1][0], k[2][0]], [k[1][1], k[2][1]], [k[1][2], k[2][2]], figure=grafik, tube_radius=0.2, color=(0.0, 0.0, 0.9), name="49")

mayavi.mlab.show()
