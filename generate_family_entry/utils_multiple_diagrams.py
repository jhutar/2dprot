#!/usr/bin/env python3
# tool which generate for given family name its directory with
# * single images
# * multi images per whole family
# * multi images for its s35 subfamilies

import argparse
import utils_common
import utils_generate_svg


def multiple_diagrams(family, detection_dir, layout_dir, first_svg, not_ligands):
    svg_args = argparse.Namespace(
        add_descriptions="multi",
        layouts=[f"{layout_dir}"],
        ligands=False,
        opac='1',
        debug=False,
        irotation=f"{detection_dir}/rotation.json",
        itemplate=None,
        iligands=f"{detection_dir}/ligands.json",
        orotation=None,
        otemplate=f"{detection_dir}/{family}.json",
        output=f"{layout_dir}/{family}.svg",
        oligands=None,
        data=None,
    )
    if not_ligands:
        svg_args.ligands = False
    if first_svg is False:
        utils_generate_svg.compute(svg_args)
    svg_args.otemplate = None
    utils_common.convert_svg_to_png(svg_args.output, svg_args.output[:-4]+".png", f"{detection_dir}", "")
    svg_args.output = f"{layout_dir}/{family}_3.svg"
    svg_args.itemplate = f"{detection_dir}/{family}.json"
    utils_generate_svg.compute(svg_args)
    utils_common.convert_svg_to_png(svg_args.output, svg_args.output[:-4]+".png", f"{detection_dir}", "")
    svg_args.output = f"{layout_dir}/{family}_2.svg"
    svg_args.opac = "0"
    utils_generate_svg.compute(svg_args)
    utils_common.convert_svg_to_png(svg_args.output, svg_args.output[:-4]+".png", f"{detection_dir}", "")
    svg_args.output = f"{layout_dir}/{family}_4.svg"
    svg_args.add_descriptions = "multi,ligands"
    svg_args.ligands = True
    svg_args.oligands = f"{layout_dir}/ligands.json"
    svg_args.opac = "0"
    utils_generate_svg.compute(svg_args)
    utils_common.convert_svg_to_png(svg_args.output, svg_args.output[:-4]+".png", f"{detection_dir}", "")
