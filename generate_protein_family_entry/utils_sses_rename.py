#!/usr/bin/env python3

def str_to_int(char):
    if char == "A" or char == "a":
        return "1"
    elif char == "B" or char == "b":
        return "2"
