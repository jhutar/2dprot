#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import sys
import pymol

sys.path.append('lib/colour/')

def get_rainbow_colors(sses,res):
    if len(sses) <= 0:
        return []
    r = {}
    min = 10000
    max = 0
    for sse in sses:
       if sse in res:
          if res[sse][0] < min:
             min = res[sse][0]
          if res[sse][1] > max:
             max = res[sse][1]
    i = 0
    coef=(max-min)
    if coef == 0:
       coef = 1

    for sse in sses:
       if sse in res:
          i = i + 1
          hr=int((res[sse][0]+res[sse][1])/2)
          color_s='s'+str(int((hr-min)*999/coef)).zfill(3)
          color_c = pymol.cmd.get_color_tuple(color_s)
          color_p = (int(color_c[0]*255), \
                   int(color_c[1]*255), \
                   int(color_c[2]*255))
          color_sse = '#%02x%02x%02x' % color_p
          r[sse]=(color_sse)
       else:
          r[sse]=('black')
    return r

