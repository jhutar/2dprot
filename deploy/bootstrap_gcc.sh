#!/bin/bash

set -e
set -x
set -u

# Check we have dssp installed as it is used in the config
if ! type gcc; then
    echo "ERROR: gcc not in the path - please install gcc package" >&2
    exit 1
fi

# Build
cd gcc
gcc -lm -o sses_main_loop -lcjson utils_matrix_sses_iteration.c
gcc -lm -o ligands_main_loop -lcjson utils_matrix_ligands_iteration.c