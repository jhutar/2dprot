#!/usr/bin/env python3
# tool which generate for given pin directory its standart graphs with up/down part charges sum

import argparse
import time
import os
import json
import svgwrite
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd


def up_part(ch_data, part1, part2, text):

    up = []
    down = []
    res_min = 10000
    res_max = 0
    for res in ch_data:
        if int(res) < res_min:
            res_min = int(res)
        if int(res) > res_max:
            res_max = int(res)
    res_middle = res_min + (res_max - res_min) / 2
    aux = []
    res = res_min
    while res < res_middle:
        aux.append(ch_data[str(res)])
        res += 1

    if part1 == "up":
        up = aux
    else:
        down = aux
    aux2 = []
    while res <= res_max:
        aux2.append(ch_data[str(res)])
        res += 1
    if part2 == "up":
        up += aux2
    else:
        down += aux2

    return up, down


def up_part2(name, data, sses_list, data_all_res, min_index, max_index, part, text):
    if name in data[sses_list[min_index]] and name in data[sses_list[max_index]]:
        res_min = 1
        for i in data[sses_list[min_index]][name]:
            if res_min is None or res_min < int(i):
                res_min = int(i)
        res_min += 1

        res_max = None
        for i in data[sses_list[max_index]][name]:
            if res_max is None or res_max > int(i):
                res_max = int(i)

        ch_data = {}
        res = res_min
        while res < res_max:
            ch_data[str(res)] = data_all_res[res]
            res += 1
        return up_part(ch_data, part, part, text)
    else:
        return [], []


def up_part_start(name, data, sses_list, data_all_res, sindex, part, text):
    if name in data[sses_list[sindex]]:
        res_min = 1000
        for i in data[sses_list[sindex]][name]:
            if res_min is None or res_min > int(i):
                res_min = int(i)

        ch_data = {}
        res = 1
        while res < res_min:
            ch_data[str(res)] = data_all_res[res]
            res += 1
        return up_part(ch_data, part, part, text)
    else:
        return [], []


def up_part_end(name, data, sses_list, data_all_res, sindex, part, text):
    if name in data[sses_list[sindex]]:
        res_max = 1
        for i in data[sses_list[sindex]][name]:
            if res_max is None or res_max < int(i):
                res_max = int(i)

        ch_data = {}
        res = res_max + 1
        while res in data_all_res:
            ch_data[str(res)] = data_all_res[res]
            res += 1
        return up_part(ch_data, part, part, text)
    else:
        return [], []


def graph_full(data, sses_order, fn_order, avg, first, data_all_res, mapping_names):
    # generate diagram and record
    name = fn_order[0]

    c_sses = 0
    for sses in sses_order:
         c_sses += 1

    #      sses_list = ["H2", "H3", "H5", "H6", "H8", "H9", "H19", "H20", "H21", "H22", "H23", "H25", "H26"] # small dataset(45 pcs)
    sses_list = ["H6", "H7", "H9", "H11", "H13", "H15", "H44", "H45", "H48", "H51", "H53", "H56", "H59"]  # BIG DATASET
    sum_up = []
    sum_down = []

    r_up, r_down = up_part_start(name, data, sses_list, data_all_res, 0, "up", name)
    sum_up += r_up
    sum_down += r_down

    r_up, r_down = up_part2(name, data, sses_list, data_all_res, 0, 1, "down", name)
    sum_up += r_up
    sum_down += r_down

    r_up, r_down = up_part2(name, data, sses_list, data_all_res, 1, 2, "up", name)
    sum_up += r_up
    sum_down += r_down

    r_up, r_down = up_part2(name, data, sses_list, data_all_res, 2, 3, "down", name)
    sum_up += r_up
    sum_down += r_down

#    r_up, r_down = up_part2(name, data, sses_list, data_all_res, 3, 4, "up", name)
#    sum_up += r_up
#    sum_down += r_down

    r_up, r_down = up_part2(name, data, sses_list, data_all_res, 4, 5, "up", name)
    sum_up += r_up
    sum_down += r_down

    # big loop
    big_loop_up, big_loop_down = up_part2(name, data, sses_list, data_all_res, 5, 6, "down", name)
#    sum_up += r_up
#    sum_down += r_down
#    pom_up += sum(r_up)
#    pom_down += sum(r_down)

#    r_up, r_down = up_part2(name, data, sses_list, data_all_res, 6, 7, "up", name)
#    sum_up += r_up
#    sum_down += r_down

    r_up, r_down = up_part2(name, data, sses_list, data_all_res, 7, 8, "up", name)
    sum_up += r_up
    sum_down += r_down

    r_up, r_down = up_part2(name, data, sses_list, data_all_res, 8, 9, "down", name)
    sum_up += r_up
    sum_down += r_down

    r_up, r_down = up_part2(name, data, sses_list, data_all_res, 9, 10, "up", name)
    sum_up += r_up
    sum_down += r_down

#    r_up, r_down = up_part2(name, data, sses_list, data_all_res, 10, 11, "up", name)
#    sum_up += r_up
#    sum_down += r_down

    r_up, r_down = up_part2(name, data, sses_list, data_all_res, 11, 12, "down", name)
    sum_up += r_up
    sum_down += r_down

    r_up, r_down = up_part_end(name, data, sses_list, data_all_res, 12, "up", name)
    sum_up += r_up
    sum_down += r_down

    sname = name.split("-")
    # print loops only charges
    return [mapping_names[name], int(sum(sum_up)), int(sum(sum_down)), sname[1], sum(big_loop_down), int(min(list(data[sses_list[6]][name].keys()))) - int(max(list(data[sses_list[5]][name].keys())))]


def avg(vlist):
    vcounter = 0
    vsum = 0
    for i in vlist:
        vcounter += 1
        vsum += i
    return(vsum /vcounter)

# read arguments
parser = argparse.ArgumentParser(
    description='generated charges special diagram - simple one')
parser.add_argument('--directory', required=True, action="store",
                    help="name of the directory, which contains domain charges layout")
parser.add_argument('--charges_dir', required=True, action="store",
                    help="charges data dir")
parser.add_argument("--name", help="name")
args = parser.parse_args()

# create new protein diagram database directory
timestamp = time.strftime("%Y-%m-%dT%H_%M_%S")
name = os.path.basename(os.path.normpath(args.directory))
new_dir = f"charges-{name}-{timestamp}"
os.makedirs(new_dir, exist_ok=True)
print("new_dir", new_dir, " - created")

data = {}      # charges data
fn_order = []
data_all_res = {}
pin_mapping = {}
for dirname in os.listdir(args.directory):
  if os.path.isdir(dirname):
      continue
  print("pin directory name:", dirname[0:4], ": ", dirname)
  for filename in os.listdir(f"{args.directory}/{dirname}"):
    pin_mapping[filename] = dirname[0:4]
    # go through all PINs in PIN directory and create record for them

    # read PIN json and read residues charges in SSEs
    print("read", f"{args.directory}/{dirname}/{filename}")
    with open(f"{args.directory}/{dirname}/{filename}", "r") as fd:
        jdata = json.load(fd)
        fn_order.append(filename)
        for sse in jdata["h_type"]:
            if jdata["h_type"][sse] == 1 and (sse[0] != "E"):
                if sse not in data:
                    data[sse] = {}
                data[sse][filename] = jdata["charges"][sse]

    # read PIN json and read residues charges in parts outside SSEs
    sfilename = filename.split("-")
    ac_filename = f"{sfilename[1]}.alphacharges"
    with open(f"{args.charges_dir}/{ac_filename}", "rb") as fd:
        ac_res = {}
        r = fd.read()

        line = ""
        start = False

        for c in r:
            if chr(c) != "\n":
                # read separate character and put them to to lines
                line = line + chr(c)
            else:
                if "ATOM" in line:
                    # the first line contains the rest of old stuff at the beginning, need to be moved
                    pos = line.find("ATOM")
                    line = line[pos:]
                if line.startswith("ATOM") and start:
                    if int(line[21:26]) not in ac_res:
                        ac_res[int(line[21:26])] = []
                    ac_res[int(line[21:26])].append(float(line[55:62]))

                line = ""
            if line.startswith("TER"):
                # read only stuff between "TER" signs
                if not start:
                    start = True
                else:
                    break

    # data aggregation
    data_all_res[filename] = {}
    for r in ac_res:
        s = 0
        for ss in ac_res[r]:
            s += ss
        data_all_res[filename][r] = s*100
    for sse in data:
        if filename in data[sse]:
            for res in data[sse][filename]:
                data[sse][filename][res] = data_all_res[filename][int(res)]

pin_mapping_order = sorted(pin_mapping.items(), key=lambda x: x)
fn_order = []

for i in pin_mapping:
    fn_order.append(i)
sses_order = list(data.keys())

# for all PINs generate the output html record and diagram
records = []

for f in fn_order:
    # generate diagram and recors
    r = graph_full(data, sses_order, [f], 2, True, data_all_res[f], pin_mapping)
    if r is not False:
        records.append(r)

const = 2
# do diagram
width = 1200
height = 1200

# draw axes
dwg = svgwrite.Drawing(f"{new_dir}/diagram-upper_loops-down_loops-{name}.svg", size=(width*const, height*const))
dwg.add(dwg.rect(insert=(1, 1), size=(width*const, height*const), fill="white"))
dwg.add(dwg.line(start=((width/2+500)*const, (height/2-500)*const), end=((width/2-500)*const, (height/2+500)*const), stroke="black", stroke_width=2))
dwg.add(dwg.line(start=((width/2-500)*const, (height/2)*const), end=((width/2+500)*const, (height/2)*const), stroke="black", stroke_width=2))
dwg.add(dwg.text(f"{0}", insert=((width/2+3)*const, (height/2-3)*const), color="white", font_size=12, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/2-500)*const, (height/2-200/2)*const), end=((width/2+500)*const, (height/2-200/2)*const), stroke="grey"))
dwg.add(dwg.text(f"{2}", insert=((width/2+3)*const, (height/2-200/2-3)*const), color="white", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/2-500)*const, (height/2-400/2)*const), end=((width/2+500)*const, (height/2-400/2)*const), stroke="grey"))
dwg.add(dwg.text(f"{4}", insert=((width/2+3)*const, (height/2-400/2-3)*const), color="white", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/2-500)*const, (height/2+200/2)*const), end=((width/2+500)*const, (height/2+200/2)*const), stroke="grey"))
dwg.add(dwg.text(f"{-2}", insert=((width/2+3)*const, (height/2+200/2-3)*const), color="white", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/2-500)*const, (height/2+400/2)*const), end=((width/2+500)*const, (height/2+400/2)*const), stroke="grey"))
dwg.add(dwg.text(f"{-4}", insert=((width/2+3)*const, (height/2+400/2-3)*const), color="white", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/2)*const, (height/2-500)*const), end=((width/2)*const, (height/2+500)*const), stroke="black", stroke_width=2))
dwg.add(dwg.text("UP - part", insert=((width/2+3)*const, (height/2-300/2-3)*const), color="white", font_size=16, style="text-anchor:center"))

dwg.add(dwg.line(start=((width/2-600/2)*const, (height/2-500)*const), end=((width/2-600/2)*const, (height/2+500)*const), stroke="grey"))
dwg.add(dwg.text(f"{-6}", insert=((width/2-600/2+3)*const, (height-10)*const), color="white", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/2-400/2)*const, (height/2-500)*const), end=((width/2-400/2)*const, (height/2+500)*const), stroke="grey"))
dwg.add(dwg.text(f"{-4}", insert=((width/2-400/2+3)*const, (height-10)*const), color="white", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/2-200/2)*const, (height/2-500)*const), end=((width/2-200/2)*const, (height/2+500)*const), stroke="grey"))
dwg.add(dwg.text(f"{-2}", insert=((width/2-200/2+3)*const, (height-10)*const), color="white", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/2-0/2)*const, (height/2-500)*const), end=((width/2-0/2)*const, (height/2+500)*const), stroke="grey"))
dwg.add(dwg.text(f"{0}", insert=((width/2+0/2+3)*const, (height-10)*const), color="white", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/2+200/2)*const, (height/2-500)*const), end=((width/2+200/2)*const, (height/2+500)*const), stroke="grey"))
dwg.add(dwg.text(f"{2}", insert=((width/2+200/2+3)*const, (height-10)*const), color="white", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/2+400/2)*const, (height/2-500)*const), end=((width/2+400/2)*const, (height/2+500)*const), stroke="grey"))
dwg.add(dwg.text(f"{4}", insert=((width/2+400/2+3)*const, (height-10)*const), color="white", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/2+600/2)*const, (height/2-500)*const), end=((width/2+600/2)*const, (height/2+500)*const), stroke="grey"))
dwg.add(dwg.text(f"{6}", insert=((width/2+600/2+3)*const, (height-10)*const), color="white", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/2+800/2)*const, (height/2-500)*const), end=((width/2+800/2)*const, (height/2+500)*const), stroke="grey"))
dwg.add(dwg.text(f"{8}", insert=((width/2+800/2+3)*const, (height-10)*const), color="white", font_size=20, style="text-anchor:center"))
dwg.add(dwg.text("DOWN - part", insert=((width/2+900/2+3)*const, (height/2-3)*const), color="white", font_size=16, style="text-anchor:center"))
pcRotate = 'rotate(45,%s, %s)' % ((width/2+600/2+3)*const, (height/2+200/2-3)*const)
pcRotate = 'rotate(45,%s, %s)' % ((width/2+400/2+3)*const, (height/2+400/2-3)*const)

dwg.add(dwg.line(start=((width/2-650)*const, (height/2-500)*const), end=((width/2+350)*const, (height/2+500)*const), stroke="#DDDDDD", stroke_width=1))
dwg.add(dwg.line(start=((width/2-600)*const, (height/2-500)*const), end=((width/2+400)*const, (height/2+500)*const), stroke="#DDDDDD", stroke_width=1))
dwg.add(dwg.line(start=((width/2-550)*const, (height/2-500)*const), end=((width/2+450)*const, (height/2+500)*const), stroke="#DDDDDD", stroke_width=1))
dwg.add(dwg.line(start=((width/2-500)*const, (height/2-500)*const), end=((width/2+500)*const, (height/2+500)*const), stroke="#DDDDDD", stroke_width=1))
dwg.add(dwg.line(start=((width/2-450)*const, (height/2-500)*const), end=((width/2+550)*const, (height/2+500)*const), stroke="#DDDDDD", stroke_width=1))
dwg.add(dwg.line(start=((width/2-400)*const, (height/2-500)*const), end=((width/2+600)*const, (height/2+500)*const), stroke="#DDDDDD", stroke_width=1))
dwg.add(dwg.line(start=((width/2-350)*const, (height/2-500)*const), end=((width/2+650)*const, (height/2+500)*const), stroke="#DDDDDD", stroke_width=1))
dwg.add(dwg.line(start=((width/2-300)*const, (height/2-500)*const), end=((width/2+700)*const, (height/2+500)*const), stroke="#DDDDDD", stroke_width=1))
dwg.add(dwg.line(start=((width/2-250)*const, (height/2-500)*const), end=((width/2+750)*const, (height/2+500)*const), stroke="#DDDDDD", stroke_width=1))
dwg.add(dwg.line(start=((width/2-200)*const, (height/2-500)*const), end=((width/2+800)*const, (height/2+500)*const), stroke="#DDDDDD", stroke_width=1))
dwg.add(dwg.line(start=((width/2-150)*const, (height/2-500)*const), end=((width/2+850)*const, (height/2+500)*const), stroke="#DDDDDD", stroke_width=1))
dwg.add(dwg.line(start=((width/2-100)*const, (height/2-500)*const), end=((width/2+900)*const, (height/2+500)*const), stroke="#DDDDDD", stroke_width=1))
dwg.add(dwg.line(start=((width/2-50)*const, (height/2-500)*const), end=((width/2+950)*const, (height/2+500)*const), stroke="#DDDDDD", stroke_width=1))
dwg.add(dwg.line(start=((width/2)*const, (height/2-500)*const), end=((width/2+1000)*const, (height/2+500)*const), stroke="#DDDDDD", stroke_width=1))
dwg.add(dwg.line(start=((width/2+50)*const, (height/2-500)*const), end=((width/2+1050)*const, (height/2+500)*const), stroke="#DDDDDD", stroke_width=1))
dwg.add(dwg.line(start=((width/2+100)*const, (height/2-500)*const), end=((width/2+1100)*const, (height/2+500)*const), stroke="#DDDDDD", stroke_width=1))
dwg.add(dwg.line(start=((width/2)*const, (height/2-500)*const), end=((width/2)*const, (height/2+500)*const), stroke="black", stroke_width=2))

csum = {}
ccounter = {}
for sline in records:
        if (sline[0] == "pin1") or (sline[0] == "PIN1"):
            color = "red"
            c = 0
        elif (sline[0] == "pin2") or (sline[0] == "PIN2"):
            color = "green"
            c = 1
        elif (sline[0] == "pin3") or (sline[0] == "PIN3"):
            color = "blue"
            c = 2
        elif (sline[0] == "pin4") or (sline[0] == "PIN4"):
            color = "pink"
            c = 3
        elif (sline[0] == "pin5") or (sline[0] == "PIN5"):
            color = "magenta"
            c = 4
        elif (sline[0] == "pin6") or (sline[0] == "PIN6"):
            color = "cyan"
            c = 5
        elif (sline[0] == "pin7") or (sline[0] == "PIN7"):
            color = "brown"
            c = 6
        elif (sline[0] == "pin8") or (sline[0] == "PIN8"):
            color = "orange"
            c = 7
        if c not in csum:
            csum[c] = 0
            ccounter[c] = 0
        csum[c] += sline[2]
        ccounter[c] += 1

        dwg.add(dwg.line(start=((width/2+int(sline[2])/2-1)*const, (height/2-int(sline[1])/2+1)*const), end=((width/2+int(sline[2])/2+2)*const, (height/2-int(sline[1])/2-2)*const), stroke_width=8, stroke=color))

print("len", len(records))


dwg.save()
width = 1200
height = 200
data_list = {}
data_list_u = {}
data_list_d = {}
data_list_bl = {}
data_list_longlength = {}
data_listd = []
data_listu = []

const = 2

dmin = []

for sline in records:
    if sline[0] not in data_list_d:
         data_list_d[sline[0]] = []
    data_list_d[sline[0]].append(sline[2])
    if sline[0] not in data_list_u:
         data_list_u[sline[0]] = []
    data_list_u[sline[0]].append(sline[1])
    if sline[0] not in data_list_bl:
         data_list_bl[sline[0]] = []
    data_list_bl[sline[0]].append(sline[4])
    if sline[0] not in data_list_longlength:
         data_list_longlength[sline[0]] = []
    data_list_longlength[sline[0]].append(sline[5])


for sline in records:
    if sline[2] < 8:
        data_listd.append({"type": sline[0], "down": sline[2]/100, "up": sline[1]/100, })

for sline in records:
    if sline[1] < 8:
        data_listu.append({"type": sline[0], "down": sline[1]/100, "up": sline[1]/100, })

# draw down small loops 1d diagram
# draw axes
dwg = svgwrite.Drawing(f"{new_dir}/diagram-down-small-loops-{name}.svg", size=(width*const, height*const))
dwg.add(dwg.rect(insert=(1, 1), size=(width*const, height*const), fill="white"))
dwg.add(dwg.line(start=(width/2*const, (30)*const), end=(width/2*const, 30*const+7*40), stroke_width=4, stroke="grey"))
dwg.add(dwg.text(f"{0}", insert=(width/2*const, (30)*const), color="grey", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/2+int(200)/2)*const, (30)*const), end=((width/2+int(200)/2)*const, 30*const+7*40), stroke_width=2, stroke="grey"))
dwg.add(dwg.text(f"{2}", insert=((width/2+int(200)/2)*const, (30)*const), color="grey", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/2+int(400)/2)*const, (30)*const), end=((width/2+int(400)/2)*const, 30*const+7*40), stroke_width=2, stroke="grey"))
dwg.add(dwg.text(f"{4}", insert=((width/2+int(400)/2)*const, (30)*const), color="grey", font_size=20, style="text-anchor:center"))

for pin in data_list_u:
        data_list_d[pin].sort()
        l5 = int(len(data_list_d[pin])/20)
        l10 = int(len(data_list_d[pin])/10)
        if (pin == "pin1") or (pin == "PIN1"):
            color = "red"
            c = 0
        elif (pin == "pin2") or (pin == "PIN2"):
            color = "green"
            c = 1
        elif (pin == "pin3") or (pin == "PIN3"):
            color = "blue"
            c = 2
        elif (pin == "pin4") or (pin == "PIN4"):
            color = "pink"
            c = 3
        elif (pin == "pin5") or (pin == "PIN5"):
            color = "magenta"
            c = 4
        elif (pin == "pin6") or (pin == "PIN6"):
            color = "cyan"
            c = 5
        elif (pin == "pin7") or (pin == "PIN7"):
            color = "brown"
            c = 6
        elif (pin == "pin8") or (pin == "PIN8"):
            color = "orange"
            c = 7
        if c not in csum:
            csum[c] = 0
            ccounter[c] = 0
        dwg.add(dwg.line(start=((width/2+int(data_list_d[pin][0])/2-3)*const, (30)*const+c*40), end=((width/2+int(data_list_d[pin][-1])/2+3)*const, (30)*const+c*40), stroke_width=1, stroke=color))
        dwg.add(dwg.line(start=((width/2+int(data_list_d[pin][l5])/2-3)*const, (30)*const+c*40), end=((width/2+int(data_list_d[pin][-1-l5])/2+3)*const, (30)*const+c*40), stroke_width=4, stroke=color))
        dwg.add(dwg.line(start=((width/2+int(data_list_d[pin][l10])/2-3)*const, (30)*const+c*40), end=((width/2+int(data_list_d[pin][-1-l10])/2+3)*const, (30)*const+c*40), stroke_width=9, stroke=color))
dwg.save()

# draw up smapp loops 1d diagram
# draw axes
dwg = svgwrite.Drawing(f"{new_dir}/diagram-up-small-loops-{name}.svg", size=(width*const, height*const))
dwg.add(dwg.rect(insert=(1, 1), size=(width*const, height*const), fill="white"))
dwg.add(dwg.line(start=((width/2-int(200)/2)*const, (30)*const), end=((width/2-int(200)/2)*const, 30*const+7*40), stroke_width=2, stroke="grey"))
dwg.add(dwg.text(f"{-2}", insert=((width/2-int(200)/2)*const, (30)*const), color="grey", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=(width/2*const, (30)*const), end=(width/2*const, 30*const+7*40), stroke_width=2, stroke="grey"))
dwg.add(dwg.text(f"{0}", insert=(width/2*const, (30)*const), color="grey", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/2+int(200)/2)*const, (30)*const), end=((width/2+int(200)/2)*const, 30*const+7*40), stroke_width=2, stroke="grey"))
dwg.add(dwg.text(f"{2}", insert=((width/2+int(200)/2)*const, (30)*const), color="grey", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/2+int(400)/2)*const, (30)*const), end=((width/2+int(400)/2)*const, 30*const+7*40), stroke_width=2, stroke="grey"))
dwg.add(dwg.text(f"{4}", insert=((width/2+int(400)/2)*const, (30)*const), color="grey", font_size=20, style="text-anchor:center"))

for pin in data_list_u:
        data_list_u[pin].sort()
        l5 = int(len(data_list_u[pin])/20)
        l10 = int(len(data_list_u[pin])/10)

        if (pin == "pin1") or (pin == "PIN1"):
            color = "red"
            c = 0
        elif (pin == "pin2") or (pin == "PIN2"):
            color = "green"
            c = 1
        elif (pin == "pin3") or (pin == "PIN3"):
            color = "blue"
            c = 2
        elif (pin == "pin4") or (pin == "PIN4"):
            color = "pink"
            c = 3
        elif (pin == "pin5") or (pin == "PIN5"):
            color = "magenta"
            c = 4
        elif (pin == "pin6") or (pin == "PIN6"):
            color = "cyan"
            c = 5
        elif (pin == "pin7") or (pin == "PIN7"):
            color = "brown"
            c = 6
        elif (pin == "pin8") or (pin == "PIN8"):
            color = "orange"
            c = 7
        if c not in csum:
            csum[c] = 0
            ccounter[c] = 0
        dwg.add(dwg.line(start=((width/2+int(data_list_u[pin][0])/2-3)*const, (30)*const+c*40), end=((width/2+int(data_list_u[pin][-1])/2+3)*const, (30)*const+c*40), stroke_width=1, stroke=color))
        dwg.add(dwg.line(start=((width/2+int(data_list_u[pin][l5])/2-3)*const, (30)*const+c*40), end=((width/2+int(data_list_u[pin][-1-l5])/2+3)*const, (30)*const+c*40), stroke_width=4, stroke=color))
        dwg.add(dwg.line(start=((width/2+int(data_list_u[pin][l10])/2-3)*const, (30)*const+c*40), end=((width/2+int(data_list_u[pin][-1-l10])/2+3)*const, (30)*const+c*40), stroke_width=9, stroke=color))
dwg.save()

# draw big loops 1d diagram
# draw axes
dwg = svgwrite.Drawing(f"{new_dir}/diagram-big-loop-{name}.svg", size=(width*const, height*const))
dwg.add(dwg.rect(insert=(1, 1), size=(width*const, height*const), fill="white"))

dwg.add(dwg.line(start=((width/2-int(400)/2)*const, (30)*const), end=((width/2-int(400)/2)*const, 30*const+7*40), stroke_width=2, stroke="grey"))
dwg.add(dwg.text(f"{-4}", insert=((width/2-int(400)/2)*const, (30)*const), color="grey", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/2-int(200)/2)*const, (30)*const), end=((width/2-int(200)/2)*const, 30*const+7*40), stroke_width=2, stroke="grey"))
dwg.add(dwg.text(f"{-2}", insert=((width/2-int(200)/2)*const, (30)*const), color="grey", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=(width/2*const, (30)*const), end=(width/2*const, 30*const+7*40), stroke_width=4, stroke="grey"))
dwg.add(dwg.text(f"{0}", insert=(width/2*const, (30)*const), color="grey", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/2+int(200)/2)*const, (30)*const), end=((width/2+int(200)/2)*const, 30*const+7*40), stroke_width=2, stroke="grey"))
dwg.add(dwg.text(f"{2}", insert=((width/2+int(200)/2)*const, (30)*const), color="grey", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/2+int(400)/2)*const, (30)*const), end=((width/2+int(400)/2)*const, 30*const+7*40), stroke_width=2, stroke="grey"))
dwg.add(dwg.text(f"{4}", insert=((width/2+int(400)/2)*const, (30)*const), color="grey", font_size=20, style="text-anchor:center"))

for pin in data_list_bl:
    for d in data_list_bl[pin]:
        data_list_bl[pin].sort()
        l5 = int(len(data_list_bl[pin])/20)
        l10 = int(len(data_list_bl[pin])/10)
#        print("pin dd", pin,"l je ", l5, l10, data_list_[pin][0], data_list_d[pin][l5], data_list_d[pin][l10], data_list_d[pin][-1-l10], data_list_d[pin][-1-l5], data_list_d[pin][-1], data_list_d[pin])
        if (pin == "pin1") or (pin == "PIN1"):
            color = "red"
            c = 0
        elif (pin == "pin2") or (pin == "PIN2"):
            color = "green"
            c = 1
        elif (pin == "pin3") or (pin == "PIN3"):
            color = "blue"
            c = 2
        elif (pin == "pin4") or (pin == "PIN4"):
            color = "pink"
            c = 3
        elif (pin == "pin5") or (pin == "PIN5"):
            color = "magenta"
            c = 4
        elif (pin == "pin6") or (pin == "PIN6"):
            color = "cyan"
            c = 5
        elif (pin == "pin7") or (pin == "PIN7"):
            color = "brown"
            c = 6
        elif (pin == "pin8") or (pin == "PIN8"):
            color = "orange"
            c = 7
        if c not in csum:
            csum[c] = 0
            ccounter[c] = 0
        dwg.add(dwg.line(start=((width/2+int(d)/2)*const-5, (30)*const+c*40-5), end=((width/2+int(d)/2)*const+5, (30)*const+c*40+5), stroke_width=2, stroke=color))
        dwg.add(dwg.line(start=((width/2+int(d)/2)*const+5, (30)*const+c*40-5), end=((width/2+int(d)/2)*const-5, (30)*const+c*40+5), stroke_width=2, stroke=color))
dwg.save()

### violin graph big loop sum charge
const = 1
width = width
height=height *2
k = 3
dwg = svgwrite.Drawing(f"{new_dir}/diagram-big-loop2-{name}.svg", size=(width*const, height*const))
dwg.add(dwg.rect(insert=(1, 1), size=(width, height), fill="white"))
print("rozmery ", width, height)

dwg.add(dwg.line(start=((width/k-int(800)/k)*const, (30)*const), end=((width/k-int(800)/k)*const, 30*const+8*40), stroke_width=2, stroke="grey"))
dwg.add(dwg.text(f"{-800}", insert=((-17+width/k-int(800)/k)*const, (30)*const-10), color="grey", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/k-int(600)/k)*const, (30)*const), end=((width/k-int(600)/k)*const, 30*const+8*40), stroke_width=2, stroke="grey"))
dwg.add(dwg.text(f"{-600}", insert=((-17+width/k-int(600)/k)*const, (30)*const-10), color="grey", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/k-int(400)/k)*const, (30)*const), end=((width/k-int(400)/k)*const, 30*const+8*40), stroke_width=2, stroke="grey"))
dwg.add(dwg.text(f"{-400}", insert=((-17+width/k-int(400)/k)*const, (30)*const-10), color="grey", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/k-int(200)/k)*const, (30)*const), end=((width/k-int(200)/k)*const, 30*const+8*40), stroke_width=2, stroke="grey"))
dwg.add(dwg.text(f"{-200}", insert=((-17+width/k-int(200)/k)*const, (30)*const-10), color="grey", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=(width/k*const, (30)*const), end=(width/k*const, 30*const+8*40), stroke_width=4, stroke="grey"))
dwg.add(dwg.text(f"{0}", insert=(width/k*const, (30)*const-10), color="grey", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/k+int(200)/k)*const, (30)*const), end=((width/k+int(200)/k)*const, 30*const+8*40), stroke_width=2, stroke="grey"))
dwg.add(dwg.text(f"{200}", insert=((-17+width/k+int(200)/k)*const, (30)*const-10), color="grey", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/k+int(400)/k)*const, (30)*const), end=((width/k+int(400)/k)*const, 30*const+8*40), stroke_width=2, stroke="grey"))
dwg.add(dwg.text(f"{400}", insert=((-17+width/k+int(400)/k)*const, (30)*const-10), color="grey", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/k+int(600)/k)*const, (30)*const), end=((width/k+int(600)/k)*const, 30*const+8*40), stroke_width=2, stroke="grey"))
dwg.add(dwg.text(f"{600}", insert=((-17+width/k+int(600)/k)*const, (30)*const-10), color="grey", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/k+int(800)/k)*const, (30)*const), end=((width/k+int(800)/k)*const, 30*const+8*40), stroke_width=2, stroke="grey"))
dwg.add(dwg.text(f"{800}", insert=((-17+width/k+int(800)/k)*const, (30)*const-10), color="grey", font_size=20, style="text-anchor:center"))

r = 80
#const = 1
for pin in data_list_bl:
    r =int(len(data_list_bl[pin])/4)
    data_list_bl[pin].sort()
    ll_min = min(data_list_bl[pin])
    ll_max = max(data_list_bl[pin])
    ll_avg = avg(data_list_bl[pin])
#    print("dlb", ll_min, data_list_bl[pin])
    last = 0
    for i in range(r):
        int_min = ll_min + (ll_max - ll_min)*i/r
        int_max = ll_min + (ll_max - ll_min)*(i+1)/r + 1
#        print(f"int_min ll_min={ll_min}, ll_max={ll_max}, ll_max-ll_min={ll_max-ll_min}, i/r={i/r}, celkem={(ll_max - ll_min)*i/r}")
        counter = 0
        for c in data_list_bl[pin]:
            if int_min <= c and c < int_max:
                counter += 1
#        print("im", int_min, int_max, counter, data_list_bl[pin])
        if (pin == "pin1") or (pin == "PIN1"):
            color = "red"
            c = 0
        elif (pin == "pin2") or (pin == "PIN2"):
            color = "green"
            c = 1
        elif (pin == "pin3") or (pin == "PIN3"):
            color = "blue"
            c = 2
        elif (pin == "pin4") or (pin == "PIN4"):
            color = "pink"
            c = 3
        elif (pin == "pin5") or (pin == "PIN5"):
            color = "magenta"
            c = 4
        elif (pin == "pin6") or (pin == "PIN6"):
            color = "cyan"
            c = 5
        elif (pin == "pin7") or (pin == "PIN7"):
            color = "brown"
            c = 6
        elif (pin == "pin8") or (pin == "PIN8"):
            color = "orange"
            c = 7
        if c not in csum:
            csum[c] = 0
            ccounter[c] = 0
        c = c + 0.5
        counter = counter*1.4*r/len(data_list_bl[pin])
#        print("counter", counter,"int_min", int_min, "int_max", int_max, "width", width/k*const, const)
        print("sourad", [int((width/k+int_min/k)*const), int((30-last)*const+c*40)])
        dwg.add(dwg.polygon([[(width/k+int_min/k)*const, (30-last)*const+c*40], [(width/k+int_min/k)*const, (30+last)*const+c*40], [(width/k+int_max/k)*const, (30+counter)*const+c*40], [(width/k+int_max/k)*const, (30-counter)*const+c*40]], fill=color, stroke=color))
        last = counter
    print("stred", [int((width/k+(ll_avg+5)/k)*const), int((30-5)*const+c*40)])
    dwg.add(dwg.line(start=((width/k+(ll_avg+5)/k)*const, 30*const+c*40), end=((width/k+(ll_avg-5)/k)*const, 30*const+c*40), fill="black", stroke="black", stroke_width=10))
dwg.save()
heigh = height /2



# draw big loops 1d diagram
# draw axes
dwg = svgwrite.Drawing(f"{new_dir}/diagram-big-loop-size-{name}.svg", size=(width*const, height*const))
dwg.add(dwg.rect(insert=(1, 1), size=(width*const, height*const), fill="white"))

dwg.add(dwg.line(start=((width/2-int(400)/2)*const, (30)*const), end=((width/2-int(400)/2)*const, 30*const+7*40), stroke_width=2, stroke="grey"))
dwg.add(dwg.text(f"{-400}", insert=((width/2-int(400)/2)*const, (30)*const-10), color="grey", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/2-int(200)/2)*const, (30)*const), end=((width/2-int(200)/2)*const, 30*const+7*40), stroke_width=2, stroke="grey"))
dwg.add(dwg.text(f"{-200}", insert=((width/2-int(200)/2)*const, (30)*const-10), color="grey", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=(width/2*const, (30)*const), end=(width/2*const, 30*const+7*40), stroke_width=4, stroke="grey"))
dwg.add(dwg.text(f"{0}", insert=(width/2*const, (30)*const-10), color="grey", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/2+int(200)/2)*const, (30)*const), end=((width/2+int(200)/2)*const, 30*const+7*40), stroke_width=2, stroke="grey"))
dwg.add(dwg.text(f"{200}", insert=((width/2+int(200)/2)*const, (30)*const-10), color="grey", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((width/2+int(400)/2)*const, (30)*const), end=((width/2+int(400)/2)*const, 30*const+7*40), stroke_width=2, stroke="grey"))
dwg.add(dwg.text(f"{400}", insert=((width/2+int(400)/2)*const, (30)*const-10), color="grey", font_size=20, style="text-anchor:center"))

for pin in data_list_longlength:
    for d in data_list_longlength[pin]:
        if (pin == "pin1") or (pin == "PIN1"):
            color = "red"
            c = 0
        elif (pin == "pin2") or (pin == "PIN2"):
            color = "green"
            c = 1
        elif (pin == "pin3") or (pin == "PIN3"):
            color = "blue"
            c = 2
        elif (pin == "pin4") or (pin == "PIN4"):
            color = "pink"
            c = 3
        elif (pin == "pin5") or (pin == "PIN5"):
            color = "magenta"
            c = 4
        elif (pin == "pin6") or (pin == "PIN6"):
            color = "cyan"
            c = 5
        elif (pin == "pin7") or (pin == "PIN7"):
            color = "brown"
            c = 6
        elif (pin == "pin8") or (pin == "PIN8"):
            color = "orange"
            c = 7
        if c not in csum:
            csum[c] = 0
            ccounter[c] = 0
        dwg.add(dwg.line(start=((width/2+int(d)/2)*const-5, (30)*const+c*40-5), end=((width/2+int(d)/2)*const+5, (30)*const+c*40+5), stroke_width=2, stroke=color))
        dwg.add(dwg.line(start=((width/2+int(d)/2)*const+5, (30)*const+c*40-5), end=((width/2+int(d)/2)*const-5, (30)*const+c*40+5), stroke_width=2, stroke=color))
dwg.save()


const = 1
# draw histogram big loop size
dwg = svgwrite.Drawing(f"{new_dir}/diagram-big-loop-size2-{name}.svg", size=(width*const, height*const))
dwg.add(dwg.rect(insert=(1, 1), size=(width*const, height*const), fill="white"))
k = 1.5
dwg.add(dwg.line(start=(10*const, (30)*const), end=(10*const, 30*const+8*40), stroke_width=4, stroke="grey"))
dwg.add(dwg.text(f"{0}", insert=(10*const-5, (30)*const-10), color="grey", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((10+int(100)/k)*const, (30)*const), end=((10+int(100)/k)*const, 30*const+8*40), stroke_width=2, stroke="grey"))
dwg.add(dwg.text(f"{100}", insert=((-2+int(100)/k)*const-5, (30)*const-10), color="grey", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((10+int(200)/k)*const, (30)*const), end=((10+int(200)/k)*const, 30*const+8*40), stroke_width=2, stroke="grey"))
dwg.add(dwg.text(f"{200}", insert=((-2+int(200)/k)*const-5, (30)*const-10), color="grey", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((10+int(300)/k)*const, (30)*const), end=((10+int(300)/k)*const, 30*const+8*40), stroke_width=2, stroke="grey"))
dwg.add(dwg.text(f"{300}", insert=((-2+int(300)/k)*const-5, (30)*const-10), color="grey", font_size=20, style="text-anchor:center"))
dwg.add(dwg.line(start=((10+int(400)/k)*const, (30)*const), end=((10+int(400)/k)*const, 30*const+8*40), stroke_width=2, stroke="grey"))
dwg.add(dwg.text(f"{400}", insert=((-2+int(400)/k)*const-5, (30)*const-10), color="grey", font_size=20, style="text-anchor:center"))

r = 80

for pin in data_list_longlength:
    r =int(len(data_list_longlength[pin])/4)
    data_list_longlength[pin].sort()
    ll_min = min(data_list_longlength[pin])
    ll_max = max(data_list_longlength[pin])
    ll_avg = avg(data_list_longlength[pin])
    print("ll_min", ll_min, ll_avg, ll_max), data_list_longlength[pin]
    last = 0
    for i in range(r):
        int_min = ll_min + (ll_max - ll_min)*i/r
        int_max = ll_min + (ll_max - ll_min)*(i+1)/r + 1
        counter = 0
        for c in data_list_longlength[pin]:
            if int_min <= c and c < int_max:
                counter += 1
        print("im", int_min, int_max, counter, data_list_longlength[pin])
        if (pin == "pin1") or (pin == "PIN1"):
            color = "red"
            c = 0
        elif (pin == "pin2") or (pin == "PIN2"):
            color = "green"
            c = 1
        elif (pin == "pin3") or (pin == "PIN3"):
            color = "blue"
            c = 2
        elif (pin == "pin4") or (pin == "PIN4"):
            color = "pink"
            c = 3
        elif (pin == "pin5") or (pin == "PIN5"):
            color = "magenta"
            c = 4
        elif (pin == "pin6") or (pin == "PIN6"):
            color = "cyan"
            c = 5
        elif (pin == "pin7") or (pin == "PIN7"):
            color = "brown"
            c = 6
        elif (pin == "pin8") or (pin == "PIN8"):
            color = "orange"
            c = 7
        if c not in csum:
            csum[c] = 0
            ccounter[c] = 0
        counter = counter*1*r/len(data_list_longlength[pin])
#        print("counter", counter, const)
#        print("sourad", [(10+int_min)*const, (30-last)*const+c*40], [(10+int_min)*const, (30+last)*const+c*40], [(10+int_max)*const, (30+counter)*const+c*40], [(10+int_max)*const, (30-counter)*const+c*40])
        dwg.add(dwg.polygon([[(10+int_min/k)*const, (30-last)*const+c*40+20], [(10+int_min/k)*const, (30+last)*const+c*40+20], [(10+int_max/k)*const, (30+counter)*const+c*40+20], [(10+int_max/k)*const, (30-counter)*const+c*40+20]], fill=color, stroke=color))
        last = counter
    print("stred", [int((10+(ll_avg+5)/k)*const), int((30-5)*const+c*40)])
    dwg.add(dwg.line(start=((10+(ll_avg+2)/k)*const, 30*const+c*40+20), end=((10+(ll_avg-2)/k)*const, 30*const+c*40+20), fill="black", stroke="black", stroke_width=10))

dwg.save()



print("data_list_bl", data_list_bl.keys())

df = pd.DataFrame(data_list_bl)
sns.violinplot(data=df, y="up", x="type", split=True, cut=0, bw=.2)
plt.show()

